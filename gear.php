<?php
ini_set('display_errors', true);

include 'StravaPHP/vendor/autoload.php';
include 'functions.php';

session_start();
$token = $_SESSION["token"];

use Strava\API\Client;
use Strava\API\Exception;
use Strava\API\Service\REST;

$adapter = new Pest('https://www.strava.com/api/v3');
$service = new REST($token, $adapter);  // Define your user token here..
$client = new Client($service);

get_gear_time($token,$client);

?>
