<?php
include 'StravaPHP/vendor/autoload.php';

use Strava\API\OAuth;
use Strava\API\Exception;

session_start();

try {
    $options = array(
        'clientId'     => 11476,
        'clientSecret' => '76109a1bd84b4d01d2a562dd498b53f387efdbe8',
        'redirectUri'  => 'http://localhost:8080/strava_gear_time/index.php'
    );
    $oauth = new OAuth($options);

    if (!isset($_GET['code'])) {
        print '<a href="'.$oauth->getAuthorizationUrl().'">connect</a>';
    } else {
        $token = $oauth->getAccessToken('authorization_code', array(
            'code' => $_GET['code']
        ));
        $_SESSION["token"] = $token;
        // print $token;
        echo '<a href="gear.php">gear</a>';
    }
} catch(Exception $e) {
    print $e->getMessage();
}
